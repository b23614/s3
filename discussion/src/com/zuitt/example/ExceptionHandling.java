package com.zuitt.example;

import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args){
        // Exceptions
            // a problem that arises during the execution of a program.
            // it disrupts the normal flow of the program and terminates it abnormally.

        // Exception Handling
        // refers to managing and catching run-time errors in order to safely run your code.
        // Errors encountered in Java:
            // "Compile-time" errors are errors that usually happen when you try to compile a program that is syntactically incorrect or has missing package imports.
            // "Run-time" errors, on the other hand, are errors that happen after compilation and during the execution of the program.
        Scanner input = new Scanner(System.in);
        int num = 0;

        System.out.println("Please enter a number: ");
        //try to do/execute the statement
        try{
            num = input.nextInt();
        }catch (Exception e){
            System.out.println("Invalid Input!");
            e.printStackTrace();
        }finally {
            System.out.println("You have entered: " + num);
        }

    }
}
