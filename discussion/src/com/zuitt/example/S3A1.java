package com.zuitt.example;

import java.util.Scanner;

public class S3A1 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int num = 1;

        long factorial = 1;

        System.out.println("Input an integer whose factorial will be computed");

        try{
            num = sc.nextInt();
            int i = 1;

            while(i <= num){
                factorial *= i;
                i++;
            }
        }catch (Exception e){
            System.out.println("Invalid Input!");
            e.printStackTrace();
        }finally {
            if(num == 0){
                System.out.println("The factorial of 0 is 1");
            }if(num < 0){
                System.out.println("The factorial of "+ num +" is 0");
            }else{
                System.out.printf("The factorial of %d is %d" , num, factorial);
            }


        }

    }
}
